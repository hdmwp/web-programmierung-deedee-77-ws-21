from django.contrib import admin
from .models import Note, TagType, Tag

# Register your models here.

admin.site.register(Note)
admin.site.register(Tag)
admin.site.register(TagType)