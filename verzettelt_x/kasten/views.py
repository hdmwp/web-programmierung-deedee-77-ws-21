from django.http.response import HttpResponse
from django.shortcuts import render, HttpResponse

from kasten.models import Note

# Create your views here.

def index(request):
    notes = Note.objects.order_by('-date_created')[:6]
   # taged_note = Note.objects.get()   
   # tags = taged_note.tags.all() ---Versuch auf tags zuzugreifen
   #{% url 'note' %}?={{note.id}}</div> ---Versuch note mittels id zu verlinken

    
    return render(request, "kasten/index.html", {'notes' : notes})
    
def note(request, pk):
    note = Note.objects.get(id=pk) 
    tags = note.tags.all()
    return render(request, 'kasten/note.html', {'note': note, 'tags':tags})


def editortest(request):
    if request.POST:
        print("Content: " + request.POST["text"])
    return render(request, "kasten/editortest.html")
