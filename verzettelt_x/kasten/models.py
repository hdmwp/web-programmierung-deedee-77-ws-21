from django.db import models
from django.db.models.fields.related import ForeignKey
from django.utils import timezone
# from colorfield.fields import ColorField

# Create your models here.

class Note(models.Model):
    title = models.CharField(max_length=150)
    content = models.TextField()
    date_created = models.DateTimeField(default=timezone.now)
    due_date = models.DateField(blank=True, null=True)

    def __str__(self):
        return f"{self.title} ({self.date_created:%Y-%m-%d %H:%M:%S}) - Due: {self.due_date}"
        


class TagType(models.Model):
    name =  models.CharField(max_length=50)
   # color = ColorField()

    def __str__(self):
        return self.name



class Tag(models.Model):
    name =  models.CharField(max_length=50)
    type = ForeignKey(TagType, on_delete=models.CASCADE)
    notes = models.ManyToManyField(Note, related_name="tags")
     
    def __str__(self):
        return self.name
